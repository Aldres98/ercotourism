import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import { store } from './redux/store'
import Profile from "./components/profile";
import { setActiveOption } from './redux/action-creators'
import Map from './components/map'
import Toggle from './components/toggle'
import Legend from './components/legend'
import Modal from './components/modal'
import RouteModal from "./components/route-modal";
import JourneyModal from "./components/journey_modal";

class Application extends React.Component {
  render() {
      return (
      <Provider store={store}>
        <div>
            <button onClick={openModal}>AAA</button>

            {/*<Profile/>*/}


            <Map />
            <Modal/>
            {/*<RouteModal/>*/}
            <JourneyModal/>

            {/*<Toggle onChange={setActiveOption} />*/}
            {/*<Legend />*/}

        </div>
      </Provider>
    );
  }

  componentDidMount(): void {

  }
}


function openModal(){
    document.getElementById("myModal").style.display = 'block'
}

ReactDOM.render(<Application />, document.getElementById('app'));
