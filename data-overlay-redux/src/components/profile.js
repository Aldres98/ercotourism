import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {fetchProfile, setUser} from "../redux/action-creators";
import profile_gude from "../img/profile_gude.png"
import Achievement from "./achievement";

let Profile = class Profile extends React.Component {

    static propTypes = {
        user: PropTypes.array.isRequired,
        ach_list: PropTypes.array.isRequired
    };

    handleClick = () => {
        console.log('значение this:', this);
        let a = this.props.fetchProfile(3);
        console.log(a)
        console.log(this.props)
        // let user = {username:"Cooler John Doe", profile_pic_url:"aa", achievments:[], role: "Гид"};
        // this.props.setUser(user);
        // console.log(this.props.user)
        // this.props.setUser(user)
    }

    componentWillMount(): void {
        this.props.fetchProfile(3);
    }


    render() {
        document.title = "Профиль " + this.props.user.name;
        console.log(this.props)




        return (


            <div className="container">
                <div className="row">
                    <div className="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
                        <div className="well profile">
                            <div className="col-sm-12">
                                <div className="col-xs-12 col-sm-8">
                                    <h2 id="user-name" onClick={this.handleClick}>{this.props.user.name || 'John Doe'}</h2>
                                    <p id="user-rank">Первопроходец</p>
                                    <p><strong>Роль: </strong>{this.props.user.is_guide == true ? "Гайд" : "Турист"}</p>
                                    <p><strong>Пройдено: </strong> 641км </p>
                                </div>
                                <div className="col-xs-12 col-sm-4 text-center item">
                                    <figure>
                                        <img
                                            src={this.props.user.role == 'Турист' ? this.props.user.profile_pic_url : "https://st4.depositphotos.com/4329009/19956/v/450/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg"}
                                            alt="" className="img-circle img-responsive"/>

                                    </figure>
                                </div>
                            </div>
                            <div className="col-xs-12 divider text-center profile-achievments-box">
                                <div className="profile-achievments-box-header">
                                    <i className="fas fa-map profile-achievments-box-icon"></i>
                                    <span className="profile-achievments-title">Достижения</span>

                                </div>
                                <div className="row achievments-row">

                                    <h1>{this.props.ach_list[0]}</h1>



                                        {/*<img src={this.props.user.ach_list[0].img ? this.props.user.ach_list[0].img : "#"} title='Достижение 2'/>*/}

                                    <div className="col-md-2 col-xs-12 achievment">
                                        {/*<img  src={this.props.loading == false ? this.props.items.ach_list[0].id : "A"} title='Достижение 3 : 10/10' />*/}
                                        <img src={profile_gude}/>
                                    </div>

                                    <div className="col-md-2 col-xs-12 achievment">
                                        <img src={profile_gude}/>
                                    </div>

                                    <div className="col-md-2 col-xs-12 achievment">
                                        <img src={profile_gude}/>
                                    </div>

                                    <div className="col-md-2 col-xs-12 achievment">
                                        <img src={profile_gude}/>
                                    </div>


                                    <div className="col-md-2 col-xs-12 achievment">
                                        <img src={profile_gude}/>
                                    </div>

                                    <div className="col-md-2 col-xs-12 achievment">
                                        <img src={profile_gude}/>
                                    </div>


                                </div>

                                </div>

                            <div className="col-xs-12 divider text-center profile-journeys-box">
                                <div className="profile-achievments-box-header">
                                    <i className="fas fa-map profile-journeys-box-icon"></i>
                                    <span className="profile-achievments-title">Походы</span>

                                </div>

                            </div>



                        </div>
                    </div>
                </div>
            </div>



    );
    }
}

function mapStateToProps(state) {
    return {
        items: state.items,
        loading: state.loading,
        user: state.user,
        ach_list: state.ach_list
    };
}


Profile = connect(mapStateToProps, {setUser, fetchProfile})(Profile);

export default Profile;
