import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {setUser} from "../redux/action-creators";

let Review = class Review extends React.Component {

    static propTypes = {
        username: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
    };


    render() {

        return (
            <div className="review-box">
                <img className="review-avatar" src="https://st4.depositphotos.com/4329009/19956/v/450/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg"/>
                <span className="review-name">{this.props.username}</span>

                <div className="review-text">
                    {this.props.text}
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

Review = connect(mapStateToProps, {setUser})(Review);

export default Review;
