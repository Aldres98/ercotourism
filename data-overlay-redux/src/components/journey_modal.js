import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import SimpleImageSlider from "react-simple-image-slider";
import {setUser} from "../redux/action-creators";
import Review from "./review";
import profile_gude from '../img/profile_gude.png'
import journey from '../img/journey.png'


let JourneyModal = class JourneyModal extends React.Component {

    static propTypes = {
        user: PropTypes.array.isRequired,
    };


    render() {

        return (

            <div id="route-modal" className="modal">
                <div className="modal-content">
                    <div className="modal-header">
                        <div className="route-modal-header-title-block">
                        </div>
                        <span className="close">&times;</span>
                    </div>
                    <div className="modal-body">
                        <div className="rating-block">
                            <button className="join-journey-button">Присоединиться</button>

                        </div>
                        <div>
                            <p></p>
                        </div>

                        <div className='modal-info-box route-modal-info-box-journey'>
                            <div className="row">
                                <div className="col-md-8 route-modal-quarter" >
                                    {/*<div className="col-md-2"><i className="fas fa-map modal-block-card-inner-block-icon"/></div>*/}
                                    {/*<div className="col-md-10"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised*/}
                                    {/*</p></div>*/}
                                    <p className="journey-big-text">Данные о походе:</p>
                                    <p>Посещение заповедника: </p>
                                    <p>Маршрут: </p>
                                    <p className="journey-big-text">Участники</p>
                                    <div className="row">

                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>
                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>
                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>
                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>
                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>
                                        <div className="col-md-2"><img className="journey-user-image" src={profile_gude}/></div>

                                    </div>
                                </div>
                                <div className="col-md-4 route-modal-quarter">
                                    <div className="col-md-12"><img className="journey-guide-image" src={profile_gude}/>
                                        <span className="journey-guides-name">Гид по имени:</span>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div className='modal-info-box modal-info-box-routes'>
                            <div className="col-md-10 route-description">
                                <p>
                                    1.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                </p>
                            </div>

                            <div className="col-md-10 route-description">
                                <p>
                                    2. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                </p>
                            </div>

                            <div className="col-md-10 route-description">
                                <p>
                                    3. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                </p>
                            </div>

                        </div>

                        <div className="row modal-block-cards-row">
                            <div className="col-md-6 modal-block-card">
                                <div className="modal-block-card-inner-block-header">
                                </div>
                                <div className="col-md-12  modal-block-card-inner-block">
                                    <p className="model-block-card-inner-block-title-text">ГИДЫ</p>
                                    <i className="fas fa-hat-cowboy modal-block-card-inner-block-icon"></i><span className="model-block-card-inner-block-title-text"></span>

                                </div>

                                <div className="col-md-12 modal-info-card-entry first-in-block">
                                    <img className="modal-guide-image" src={profile_gude}/>
                                    <span className="modal-guide-name">Petuhan Petuhanich</span>
                                </div>
                                <div className="col-md-12 modal-info-card-entry" >
                                    <img className="modal-guide-image" src={profile_gude}/>
                                    <span className="modal-guide-name">Petuhan Petuhanich</span>
                                </div>
                                <div className="col-md-12 modal-info-card-entry">
                                    <img className="modal-guide-image" src={profile_gude}/>
                                    <span className="modal-guide-name">Petuhan Petuhanich</span>
                                </div>

                            </div>

                            <div className="col-md-6 modal-block-card">
                                <div className="col-md-12 modal-block-card-inner-block">
                                    <p className="model-block-card-inner-block-title-text">ПОХОДЫ</p>
                                    <i className="fas fa-campground modal-block-card-inner-block-icon"></i><span className="model-block-card-inner-block-title-text"></span>
                                </div>
                                <div className="col-md-12 modal-info-card-entry first-in-block">
                                    <img className="modal-guide-image" src={profile_gude}/>
                                </div>
                                <div className="col-md-12 modal-info-card-entry" >
                                    <img className="modal-guide-image" src={profile_gude}/>
                                </div>
                                <div className="col-md-12 modal-info-card-entry">
                                    <img className="modal-guide-image" src={profile_gude}/>
                                </div>

                            </div>

                        </div>



                        <div className="comment-box-wrapper">

                            <textarea className="form-control rounded-0" id="exampleFormControlTextarea1" rows="10" placeholder="Добавить отзыв">
                        </textarea>
                            <button className="send-review-button">Отправить</button>

                        </div>

                        {/*<div><Review/></div>*/}
                        {/*<div><Review/></div>*/}
                        {/*<div><Review/></div>*/}


                    </div>

                    <div className="modal-footer">
                        <h3>Modal Footer</h3>
                    </div>
                </div>
            </div>
        );
    }
}

window.onclick = function(event) {
    if (event.target == document.getElementById('myModal')) {
        document.getElementById('myModal').style.display = "none";
    }
}

const images = [
    { url: "https://static01.nyt.com/images/2016/09/28/us/17xp-pepethefrog_web1/28xp-pepefrog-articleLarge.jpg?quality=75&auto=webp&disable=upscale" },
    { url: "https://ichef.bbci.co.uk/news/1024/branded_news/16620/production/_91408619_55df76d5-2245-41c1-8031-07a4da3f313f.jpg" },
    { url: "https://ca-times.brightspotcdn.com/dims4/default/3db78d2/2147483647/strip/true/crop/1280x1313+0+0/resize/840x862!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2Fb0%2F0d%2Fad403cece4566024bb808fd43bfe%2Fla-1476056560-snap-photo" },
    { url: "https://media1.tenor.com/images/4356ea2dd7c376f40b770cc2676fc023/tenor.gif?itemid=15154684" },
];







function mapStateToProps(state) {
    return {
        routes_info: state.routes_info,
        user: state.user
    };
}

JourneyModal = connect(mapStateToProps, {setUser})(JourneyModal);

export default JourneyModal;
