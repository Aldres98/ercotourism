import React from 'react'
import PropTypes, {func} from 'prop-types'
import mapboxgl from 'mapbox-gl'
import { connect } from 'react-redux'
import {
  fetchPictures, fetchReserveGuidesInfo, fetchReserveRoutes,
  fetchReserves,
  fetchReservesInfo,
  fetchReservesReviews
} from "../redux/action-creators";

mapboxgl.accessToken = 'pk.eyJ1Ijoiemh1a3ZvazQwMTAiLCJhIjoiY2sxczBubGVtMDcwbjNnb3o0MjVyYndjeCJ9.wO_VEutNFxbOtAo287bMWA';

let Map = class Map extends React.Component {
  mapRef = React.createRef();
  map;

  static propTypes = {
    data: PropTypes.object.isRequired,
    active: PropTypes.object.isRequired,
    reserves: PropTypes.object.isRequired,
    reservesInfo: PropTypes.object.isRequired
  };

  componentDidUpdate() {
  }

  componentWillMount(): void {
    this.props.fetchReserves();
    this.props.fetchReservesInfo(9);
    this.props.reservesInfo.id = 9

  }

  componentDidMount() {
    this.map = new mapboxgl.Map({
      container: this.mapRef.current,
      style: 'mapbox://styles/zhukvok4010/ck8kj0bnw09v51iqqlixn1qbj\n',
      center: [43.616781, 45.226014],
      zoom: 6
    });

    this.map.on('load', () => {
      this.map.addSource('reserves', {
        type: 'geojson',
        data: this.props.reserves
      });

      this.map.addLayer({
        id: 'reserves',
        type: 'fill',
        source: 'reserves',
        paint: {
          'fill-color': '#62B062',
          'fill-opacity': 0.5,
          'fill-outline-color': '#244924'

        },



      }); // ID metches `mapbox/streets-v9`

      this.map.addLayer({
        id: 'reserves-line',
        type: 'line',
        source: 'reserves',
        paint: {
          'line-color': '#62B062',
          'line-opacity': 1,
          'line-width': 3


        },



      }); // ID metches `mapbox/streets-v9`

      let popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
      });

      let _this = this;

      this.map.on('mouseenter', 'reserves', function(e)  {
        let coordinates = e.lngLat;
        let description = e.features[0].properties.name;
        _this.map.getCanvas().style.cursor = 'pointer'

        popup
            .setLngLat(coordinates)
            .setHTML(description)
            .addTo(_this.map);
      });


      this.map.on('click', 'reserves', function (e) {
        let description = e.features[0].id;
        _this.props.reservesInfo["id"] = description;
        console.log(description);
        _this.props.fetchReservesInfo(description);
        _this.props.fetchReservesReviews(description);
        _this.props.fetchReserveRoutes(description);
        _this.props.fetchReserveGuidesInfo(description)
        document.getElementById("myModal").style.display = 'block'

      });

      this.map.on('click', 'reserves-line', function (e) {
        let description = e.features[0].id;
        _this.props.reservesInfo.id = description
        console.log(description);
        _this.props.fetchReservesInfo(description);
        _this.props.fetchReservesReviews(description);
        _this.props.fetchReserveRoutes(description);
        _this.props.fetchReserveGuidesInfo(description)

        document.getElementById("myModal").style.display = 'block'

      });

    })
  }


  render() {
    return (
      <div ref={this.mapRef} className="absolute top right left bottom" />
    );
  }
};

function mapStateToProps(state) {
  return {
    title: state.title,
    reservesInfo: state.reservesInfo,
    reserves: state.reserves,
    data: state.data,
    active: state.active
  };
}

Map = connect(mapStateToProps, {fetchReserves, fetchReservesInfo, fetchPictures, fetchReservesReviews, fetchReserveRoutes, fetchReserveGuidesInfo})(Map);

export default Map;
