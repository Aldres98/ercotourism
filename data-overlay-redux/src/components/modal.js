import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import SimpleImageSlider from "react-simple-image-slider";
import {
    fetchPictures, fetchReserveGuidesInfo,
    fetchReserveRoutes,
    fetchReserveRoutesInfo,
    fetchReservesReviews,
    setUser
} from "../redux/action-creators";
import Review from "./review";
import profile_gude from '../img/profile_gude.png'


let Modal = class Modal extends React.Component {

    static propTypes = {
        user: PropTypes.array.isRequired,
    };

    genReviewCards(reviews){
        this.props.reservesInfo.content = [];
        let components = [];
        if(reviews.length > 0 ) {
            for (let i = 0; i < reviews.length; i++) {
                components.push(<Review username={reviews[i].user}
                                        text={reviews[i].text}/>);
            }
        }
        this.props.reservesInfo.content = components;
    }

    genRoutesCards(routes) {
        this.props.routes.cards = [];
        let components = [];
        if(routes.length > 0 ) {
            console.log(routes.length)
            for (let i = 0; i < routes.length; i++) {
                if(i == 3) break;
                components.push(<div className={i==0 ? "col-md-12 modal-info-card-entry first-in-block" : "col-md-12 modal-info-card-entry"}>
                    <img className="modal-guide-image" src={profile_gude}/>
                    <span className="modal-guide-name" data-id={routes[i].id} onClick={(event) => { this.handleClick(event) }} defaultValue={this.props}>{routes[i].name}</span>
                </div>);
            }
        }
        this.props.routes.cards = components;

    }

    genGuidesCards(guides) {
        this.props.guides_info.cards = [];
        console.log(guides)
        let components = [];
        if(guides.length > 0 ) {
            console.log(guides.length)
            for (let i = 0; i < guides.length; i++) {
                if(i == 3) break;
                components.push(<div className={i==0 ? "col-md-12 modal-info-card-entry first-in-block" : "col-md-12 modal-info-card-entry"}>
                    <img className="modal-guide-image" src={profile_gude}/>
                    <span className="modal-guide-name" data-id={guides[i].name}>{guides[i].name}, рейтинг: {guides[i].score}</span>
                </div>);
            }
        }
        this.props.guides_info.cards = components;

    }




    handleSubmit(event) {
        event.preventDefault();
        console.log(this.props.reservesInfo)

        fetch('http://167.172.170.134:8000/api/reserves-reviews/', {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                "rating": 4,
                "text": this.props.reservesInfo.comment,
                "reserve": this.props.reservesInfo.id,
                "user": 3,
                "pictures": []
            }),
        });
        // fetchReservesReviews(this.props.reservesInfo.id);
        // this.genReviewCards(this.props.reviews[0])
    }

    handleClick(event) {
        event.preventDefault();
        this.props.fetchReserveRoutesInfo(event.target.getAttribute('data-id'))
        openRouteModal();
    }



    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
    }


    render() {

        console.log(this.props)

        let resInfo = this.props.reservesInfo;


        const images = [
            {},
            { url: "https://ichef.bbci.co.uk/news/1024/branded_news/16620/production/_91408619_55df76d5-2245-41c1-8031-07a4da3f313f.jpg" },
            { url: "https://ca-times.brightspotcdn.com/dims4/default/3db78d2/2147483647/strip/true/crop/1280x1313+0+0/resize/840x862!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2Fb0%2F0d%2Fad403cece4566024bb808fd43bfe%2Fla-1476056560-snap-photo" },
            { url: "https://media1.tenor.com/images/4356ea2dd7c376f40b770cc2676fc023/tenor.gif?itemid=15154684" },
        ];


        return (


            <div id="myModal" className="modal">
                <div className="modal-content">
                    <div className="modal-header">
                        <div className="modal-header-title-block">
                            <span className="title-oopt-text">{resInfo.name}</span>
                        </div>
                        <span className="close">&times;</span>
                    </div>
                    <div className="modal-body">
                        <div className="rating-block">
                            <span className="rating-title">Рейтинг: </span>
                            <span className="fas fa-tree rating-icon rating-icon-checked"></span>
                            <span className="fas fa-tree rating-icon rating-icon-checked"></span>
                            <span className="fas fa-tree rating-icon rating-icon-checked"></span>
                            <span className="fas fa-tree rating-icon rating-icon-checked"></span>
                            <span className="fas fa-tree rating-icon rating-icon-unchecked"></span>

                        </div>
                        <div>
                        <p></p>
                        </div>
                        <div className='modal-info-box'>
                            <p className='modal-info-text'>{resInfo.descr}</p>
                        </div>


                            <div className="row modal-block-cards-row">
                                <div className="col-md-4 modal-block-card">
                                    <div className="modal-block-card-inner-block-header">
                                    </div>
                                    <div className="col-md-12  modal-block-card-inner-block ">
                                        <i className="fas fa-hat-cowboy modal-block-card-inner-block-icon"></i><span className="model-block-card-inner-block-title-text">ГИДЫ</span>
                                    </div>

                                    {this.props.guides_info.length > 0 ? this.genGuidesCards(this.props.guides_info) : 0}
                                    {this.props.guides_info.cards}

                                </div>
                                <div className="col-md-4  modal-block-card">
                                    <div className="col-md-12 modal-block-card-inner-block">
                                        <i className="fas fa-map modal-block-card-inner-block-icon"></i><span className="model-block-card-inner-block-title-text">МАРШРУТЫ</span>
                                    </div>

                                    {this.props.routes.length > 0 && this.props.routes[0].length ? this.genRoutesCards(this.props.routes[0]) : 0}
                                    {this.props.routes.cards}


                                </div>
                                <div className="col-md-4 modal-block-card">
                                    <div className="col-md-12 modal-block-card-inner-block">
                                        <i className="fas fa-campground modal-block-card-inner-block-icon"></i><span className="model-block-card-inner-block-title-text">ПОХОДЫ</span>
                                    </div>
                                    <div className="col-md-12 modal-info-card-entry first-in-block">
                                        <img className="modal-guide-image" src={profile_gude}/>
                                    </div>
                                    <div className="col-md-12 modal-info-card-entry" >
                                        <img className="modal-guide-image" src={profile_gude}/>
                                    </div>
                                    <div className="col-md-12 modal-info-card-entry">
                                        <img className="modal-guide-image" src={profile_gude}/>
                                    </div>

                                </div>

                                </div>




                        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img className="d-block w-100"
                                         src={this.props.loading == false ? this.props.reservesInfo.pictures[0].picture_url : "A"}
                                         alt="First slide"/>
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100"
                                         src={this.props.loading == false ? this.props.reservesInfo.pictures[1].picture_url : "A"}
                                         alt="Second slide"/>
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100"
                                         src={this.props.loading == false ? this.props.reservesInfo.pictures[2].picture_url : "A"}
                                         alt="Third slide"/>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselExampleControls" role="button"
                               data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselExampleControls" role="button"
                               data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>

                        <div className="comment-box-wrapper">
                            <form id="form1" onSubmit={(event) => { this.handleSubmit(event) }} defaultValue={this.props}>
                            <textarea className="form-control rounded-0" id="comment-text" name="comment-text" rows="10" placeholder="Добавить отзыв"
                                      onChange={(event)=>{
                                    this.props.reservesInfo["comment"] = event.target.value

                            }}>
                        </textarea>
                            <button type="submit" form="form1" className="send-review-button">Отправить</button>
                            </form>

                    </div>

                        {this.props.reviews.length > 0 && this.props.reviews[0].length ? this.genReviewCards(this.props.reviews[0]) : 0}
                        {this.props.reservesInfo.content}


                    {/*<Review username={this.props.reviews.length > 0 ? this.props.reviews[0][0].user : "iii"} text={this.props.reviews.length > 0 ? this.props.reviews[0][0].text : "S"}/>*/}


                        {/*<div><Review/></div>*/}
                        {/*<div><Review/></div>*/}
                        {/*<div><Review/></div>*/}


                    </div>

                    <div className="modal-footer">
                        <h3>Modal Footer</h3>
                    </div>
                </div>
            </div>
        );
    }
}

window.onclick = function(event) {
    if (event.target == document.getElementById('myModal')) {
        document.getElementById('myModal').style.display = "none";
    }
}







function mapStateToProps(state) {
    return {
        guides_info: state.guides_info,
        routes_info: state.routes_info,
        r_loading: state.r_loading,
        routes: state.routes,
        reviews: state.reviews,
        user: state.user,
        loading: state.loading,
        pic_loading: state.pic_loading,
        pictures: state.pictures,
        reservesInfo: state.reservesInfo
    };
}

function openRouteModal(){
    document.getElementById("route-modal").style.display = 'block'
    document.getElementById("myModal").style.display = 'none'

}


Modal = connect(mapStateToProps, {setUser, fetchPictures, fetchReservesReviews, fetchReserveRoutesInfo})(Modal);

export default Modal;
