import {applyMiddleware, compose, createStore} from 'redux'
import {initialState, reducer} from './reducer'
import thunk from 'redux-thunk';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, initialState,composeEnhancer(applyMiddleware(thunk)));

export { store };
