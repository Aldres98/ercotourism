import * as Constants from '../constants'
import data from '../data.json'
import {FETCH_PROFILE_FAILURE} from "../constants";
import {FETCH_PROFILE_SUCCESS} from "../constants";
import {FETCH_PROFILE_BEGIN} from "../constants";
import {FETCH_RESERVES_BEGIN} from "../constants";
import {FETCH_RESERVES_SUCCESS} from "../constants";
import {FETCH_RESERVES_FAILURE} from "../constants";
import {FETCH_RESERVES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVES_INFO_FAILURE} from "../constants";
import {FETCH_PICTURES_BEGIN} from "../constants";
import {FETCH_PICTURES_SUCCESS} from "../constants";
import {FETCH_PICTURES_FAILURE} from "../constants";
import {FETCH_RESERVE_REVIEWS_BEGIN} from "../constants";
import {FETCH_RESERVE_REVIEWS_FAILURE} from "../constants";
import {FETCH_RESERVE_REVIEWS_SUCCESS} from "../constants";
import {FETCH_RESERVE_ROUTES_BEGIN} from "../constants";
import {FETCH_RESERVE_ROUTES_SUCCESS} from "../constants";
import {FETCH_RESERVE_ROUTES_FAILURE} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_FAILURE} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_FAILURE} from "../constants";



const initialState: State = {
  user: {
    username: "John Doe",
    profile_pic_url: "https://clinicaesperanza.org/wp-content/uploads/2019/09/profile-placeholder.png",
    role: 'Турист',
    ach_list: [{img: "#"}]
  },
  data,
  guides_info: [],
  r_loading: true,
  reserves: [],
  reviews: [],
  routes: [],
  reservesInfo: [],
  pictures: [{picture_url: "#"}],
  ach_list: [],
  items: [],
  routes_info: [],
  pic_loading: true,
  loading: true,
  error: null
};



function reducer(state = initialState, action) {
  switch (action.type) {
    case Constants.SET_ACTIVE_OPTION:
      return Object.assign({}, state, {
        active: action.option,
        title: action.title
      });
    case Constants.SET_TITLE:
      return Object.assign(  {}, state, {
        title: action.title
      });
    case Constants.SET_USER_PROFILE:
      return {
        ...state,
        user: action.user,
      };
    case FETCH_PROFILE_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_PROFILE_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        items: action.payload.products
      };

    case FETCH_PROFILE_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_RESERVES_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_RESERVES_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        reserves: action.payload.reserves
      };

    case FETCH_RESERVES_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_RESERVES_INFO_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_RESERVES_INFO_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        reservesInfo: action.payload.reservesInfo
      };

    case FETCH_RESERVES_INFO_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_PICTURES_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        pic_loading: true,
        error: null
      };

    case FETCH_PICTURES_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        pic_loading: false,
        pictures: [action.payload.pictures]
      };

    case FETCH_PICTURES_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        pic_loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_RESERVE_REVIEWS_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        rev_loading: true,
        error: null
      };

    case FETCH_RESERVE_REVIEWS_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        rev_loading: false,
        reviews: [action.payload.reviews]
      };

    case FETCH_RESERVE_REVIEWS_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        rev_loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_RESERVE_ROUTES_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        r_loading: true,
        error: null
      };

    case FETCH_RESERVE_ROUTES_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        r_loading: false,
        routes: [action.payload.routes]
      };

    case FETCH_RESERVE_ROUTES_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        r_loading: false,
        error: action.payload.error,
        items: []
      };

    case FETCH_RESERVE_ROUTES_INFO_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        r_loading: true,
        error: null
      };

    case FETCH_RESERVE_ROUTES_INFO_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        r_loading: false,
        routes_info: action.payload.routes_info
      };

    case FETCH_RESERVE_ROUTES_INFO_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        r_loading: false,
        error: action.payload.error,
        items: []
      };
    case FETCH_RESERVE_GUIDES_INFO_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        r_loading: true,
        error: null
      };

    case FETCH_RESERVE_GUIDES_INFO_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        r_loading: false,
        guides_info: action.payload.guides_info
      };

    case FETCH_RESERVE_GUIDES_INFO_FAILURE:
      // The request failed. It's done. So set loading to "false".
      // Save the error, so we can display it somewhere.
      // Since it failed, we don't have items to display anymore, so set `items` empty.
      //
      // This is all up to you and your app though:
      // maybe you want to keep the items around!
      // Do whatever seems right for your use case.
      return {
        ...state,
        r_loading: false,
        error: action.payload.error,
        items: []
      };


    default:
      return state;
  }
}

export { reducer, initialState };
