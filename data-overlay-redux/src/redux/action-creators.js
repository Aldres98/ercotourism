import { store } from './store'
import * as Constants from '../constants'
import {FETCH_PROFILE_BEGIN} from "../constants";
import {FETCH_PROFILE_SUCCESS} from "../constants";
import {FETCH_PROFILE_FAILURE} from "../constants";
import {FETCH_RESERVES_BEGIN} from "../constants";
import {FETCH_RESERVES_SUCCESS} from "../constants";
import {FETCH_RESERVES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVES_INFO_FAILURE} from "../constants";
import {FETCH_PICTURES_BEGIN} from "../constants";
import {FETCH_PICTURES_SUCCESS} from "../constants";
import {FETCH_RESERVE_REVIEWS_BEGIN} from "../constants";
import {FETCH_RESERVE_REVIEWS_SUCCESS} from "../constants";
import {FETCH_RESERVE_REVIEWS_FAILURE} from "../constants";
import {FETCH_RESERVE_ROUTES_BEGIN} from "../constants";
import {FETCH_RESERVE_ROUTES_SUCCESS} from "../constants";
import {FETCH_RESERVE_ROUTES_FAILURE} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVE_ROUTES_INFO_FAILURE} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_SUCCESS} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_BEGIN} from "../constants";
import {FETCH_RESERVE_GUIDES_INFO_FAILURE} from "../constants";

export function setActiveOption(option, title) {
  store.dispatch({
    type: Constants.SET_ACTIVE_OPTION,
    option,
    title
  });
}

export function setTitle(title){
  store.dispatch({
    type: Constants.SET_TITLE,
    title
  });
}

export const fetchProfileBegin = () => ({
    type: FETCH_PROFILE_BEGIN
});

export const fetchProfileSuccess = products => ({
    type: FETCH_PROFILE_SUCCESS,
    payload: { products }
});

export const fetchProfileFailure = error => ({
    type: FETCH_PROFILE_FAILURE,
    payload: { error }
});

export const fetchReservesBegin = () => ({
    type: FETCH_RESERVES_BEGIN
});

export const fetchReservesSuccess = reserves => ({
    type: FETCH_RESERVES_SUCCESS,
    payload: { reserves }
});

export const fetchReservesFailure = error => ({
    type: FETCH_PROFILE_FAILURE,
    payload: { error }
});

export const fetchReservesInfoBegin = () => ({
    type: FETCH_RESERVES_INFO_BEGIN
});

export const fetchReservesInfoSuccess = reservesInfo => ({
    type: FETCH_RESERVES_INFO_SUCCESS,
    payload: { reservesInfo }
});

export const fetchReservesInfoFailure = error => ({
    type: FETCH_RESERVES_INFO_FAILURE,
    payload: { error }
});

export const fetchPicturesBegin = () => ({
    type: FETCH_PICTURES_BEGIN
});

export const fetchPicturesSuccess = pictures => ({
    type: FETCH_PICTURES_SUCCESS,
    payload: { pictures }
});

export const fetchPicturesFailure = error => ({
    type: FETCH_PROFILE_FAILURE,
    payload: { error }
});

export const fetchReserveReviewsBegin = () => ({
    type: FETCH_RESERVE_REVIEWS_BEGIN
});

export const fetchReserveReviewsSuccess = reviews => ({
    type: FETCH_RESERVE_REVIEWS_SUCCESS,
    payload: { reviews }
});

export const fetchReserveReviewsFailure = error => ({
    type: FETCH_RESERVE_REVIEWS_FAILURE,
    payload: { error }
});

export const fetchReserveRoutesBegin = () => ({
    type: FETCH_RESERVE_ROUTES_BEGIN
});

export const fetchReserveRoutesSuccess = routes => ({
    type: FETCH_RESERVE_ROUTES_SUCCESS,
    payload: { routes }
});

export const fetchReserveRoutesFailure = error => ({
    type: FETCH_RESERVE_ROUTES_FAILURE,
    payload: { error }
});

export const fetchReserveRoutesInfoBegin = () => ({
    type: FETCH_RESERVE_ROUTES_INFO_BEGIN
});

export const fetchReserveRoutesInfoSuccess = routes_info => ({
    type: FETCH_RESERVE_ROUTES_INFO_SUCCESS,
    payload: { routes_info }
});

export const fetchReserveRoutesInfoFailure = error => ({
    type: FETCH_RESERVE_ROUTES_INFO_FAILURE,
    payload: { error }
});

export const fetchReserveGuidesBegin = () => ({
    type: FETCH_RESERVE_GUIDES_INFO_BEGIN
});

export const fetchReserveGuidesSuccess = guides_info => ({
    type: FETCH_RESERVE_GUIDES_INFO_SUCCESS,
    payload: { guides_info }
});

export const fetchReserveGuidesFailure = error => ({
    type: FETCH_RESERVE_GUIDES_INFO_FAILURE,
    payload: { error }
});

export function fetchReserveGuidesInfo(id) {
    return dispatch => {
        dispatch(fetchReserveGuidesBegin());
        return fetch("http://167.172.170.134:8000/api/all-profiles?is_guide=true&known_routes=" + id)
            .then(res => res.json())
            .then(json => {
                // json.properties.id = id;
                console.log(json);
                // for(let i = 0; i < json.features.length; i++){
                //     let b = {id: json.features[i].id, name: json.features[i].properties.name};
                //     a.push(b);
                // }

                dispatch(fetchReserveGuidesSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchReserveGuidesFailure(error)));
    };
}



export function fetchReserveRoutesInfo(id) {
    return dispatch => {
        dispatch(fetchReserveRoutesInfoBegin());
        return fetch("http://167.172.170.134:8000/api/routes/id/" + id)
            .then(res => res.json())
            .then(json => {
                // json.properties.id = id;
                console.log(json);
                console.log(json.properties)
                // for(let i = 0; i < json.features.length; i++){
                //     let b = {id: json.features[i].id, name: json.features[i].properties.name};
                //     a.push(b);
                // }

                dispatch(fetchReserveRoutesInfoSuccess(json.properties));
                return json.properties;
            })
            .catch(error => dispatch(fetchReserveRoutesInfoFailure(error)));
    };
}



export function fetchReserveRoutes(id) {
    return dispatch => {
        let a = [];
        let b = []
        a["id"] = []
        dispatch(fetchReserveRoutesBegin());
        return fetch("http://167.172.170.134:8000/api/routes?reserve=" + id)
            .then(res => res.json())
            .then(json => {
                // json.properties.id = id;
                console.log(json);
                console.log(json.features)
                for(let i = 0; i < json.features.length; i++){
                    let b = {id: json.features[i].id, name: json.features[i].properties.name};
                    a.push(b);
                }

                console.log(a)
                dispatch(fetchReserveRoutesSuccess(a));
                return a;
            })
            .catch(error => dispatch(fetchReserveRoutesFailure(error)));
    };
}



export function fetchReserves() {
    return dispatch => {
        let ach_list = [];
        dispatch(fetchReservesBegin());
        return fetch("http://167.172.170.134:8000/api/reserves/")
            .then(res => res.json())
            .then(json => {
                dispatch(fetchReservesSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchReservesFailure(error)));
    };
}

export function fetchReservesInfo(id) {
    return dispatch => {
        dispatch(fetchReservesInfoBegin());
        return fetch("http://167.172.170.134:8000/api/reserves/id/" + id)
            .then(res => res.json())
            .then(json => {
                json.properties.id = id;
                dispatch(fetchReservesInfoSuccess(json.properties));
                // for(let pic = 0; pic <= json.properties.pictures.length; pic++){
                //     dispatch(fetchPictures(json.properties.pictures[pic]));
                // }
                return json;
            })
            .catch(error => dispatch(fetchReservesInfoFailure(error)));
    };
}

export function fetchReservesReviews(id) {
    return dispatch => {
        dispatch(fetchReserveReviewsBegin());
        return fetch("http://167.172.170.134:8000/api/reserves-reviews/?reserve=" + id)
            .then(res => res.json())
            .then(json => {
                // json.properties.id = id;
                dispatch(fetchReserveReviewsSuccess(json));
                // for(let pic = 0; pic <= json.properties.pictures.length; pic++){
                //     dispatch(fetchPictures(json.properties.pictures[pic]));
                // }
                return json;
            })
            .catch(error => dispatch(fetchReserveReviewsFailure(error)));
    };
}


export function fetchPictures(id) {
    let pics = [];
    return dispatch => {
        dispatch(fetchPicturesBegin());
        console.log(id)
        for(let i = 0; i <= id.length; i++) {
            fetch("http://167.172.170.134:8000/api/pictures/id/" + id[i])
                .then(res => res.json())
                .then(json => {
                    console.log(json)
                    pics.push(json)
                    return json;
                })
        }
        console.log(pics);
        dispatch(fetchPicturesSuccess(pics));
        return id;
    };
}




export function fetchProfile(id) {
  return dispatch => {
    let ach_list = [];
    dispatch(fetchProfileBegin());
    return fetch("http://167.172.170.134:8000/api/profiles?id=" + id)
        .then(res => res.json())
        .then(json => {
          for (let ach_id = 0; ach_id <= json.achievements.length; ach_id++) {
            fetch("http://167.172.170.134:8000/api/achievements?id=" + json.achievements[ach_id]).then(res => res.json())
                .then(json_ach => {
                        ach_list.push(json_ach[0])
                })
          }
          json['ach_list'] = ach_list
            console.log(json)
            dispatch(fetchProfileSuccess(json)).then(dispatch(setUser(json))
        );
          return json;
        })
        .catch(error => dispatch(fetchProfileFailure(error)));
  };
}





export function setUser(user,){
  return function(dispatch){
    dispatch({type: Constants.SET_USER_PROFILE,
        user
    })
  }

}